#!/usr/bin/env bash

npm install --save-dev \
              gulp\
              gulp-autoprefixer\
              gulp-uglify\
              gulp-stylus\
              gulp-jade\
              gulp-sourcemaps\
              rimraf
