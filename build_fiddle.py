#!/usr/bin/env python3
import argparse
import configparser
import collections
import os
import sys
import shutil
import re

PROG = "build_fiddle"
REQUIRED = (
    "project_name", "project_version", "project_licence",
    "username", "user_email",
    "src_name", "dist_name", "partials_name",
    "global_deps", "dev_deps"
)
FILES = ("bower.json", "package.json", "gulpfile.js", "README.md", ".gitignore", ".bowerrc")
MKDIRS = (
    "{src_name}/fonts",
    "{src_name}/img",
    "{src_name}/js/{partials_name}",
    "{src_name}/stylus/{partials_name}",
    "{src_name}/jade/{partials_name}",
    "{dist_name}/fonts",
    "{dist_name}/img",
    "{dist_name}/js",
    "{dist_name}/css"
)
TOUCH = (
    "{src_name}/js/main.js",
)
MV = (
    ("_base.jade", "{src_name}/jade/_base.jade"),
    ("index.jade", "{src_name}/jade/index.jade"),
    ("styles.styl", "{src_name}/stylus/styles.styl")
)

def validate_conf(conf):
    if not os.path.isfile(conf):
        msg = "Invalid config file. Check '{}' for existence".format(conf)
        raise argparse.ArgumentTypeError(msg)
    return conf


CONFIG_O = ("-c", "--config", "--set-config")
CONFIG_S = {
    "help": "explicit path to config ini-file",
    "metavar": "CONFIG FILE",
    "dest": "conf",
    "default": "build_fiddle_config.ini",
    "type": validate_conf
}
CONFIG_SECTION_O = ("-s", "--config-section")
CONFIG_SECTION_S = {
    "help": (
        "additional config section (each additional"
        " option will rewrite original one from config's [DEFAULT] section)"
    ),
    "metavar": "CONFIG SECTION",
    "dest": "section",
    "default": "DEFAULT"
}


def main():
    
    parser = argparse.ArgumentParser(prog=PROG)
    parser.add_argument(*CONFIG_O, **CONFIG_S)
    parser.add_argument(*CONFIG_SECTION_O, **CONFIG_SECTION_S)
    args = parser.parse_args()
    
    config = configparser.ConfigParser(
        strict=True, empty_lines_in_values=False,
        delimiters=("=",), comment_prefixes=("#", "//", ";")
    )
    config.read(args.conf)
    
    parsed_config = dict()
    if "DEFAULT" in config:
        parsed_config.update(config["DEFAULT"])
    if args.section != "DEFAULT":
        if args.section not in config:
            msg = "There's no '{0}' section in '{1}' config".format(
                args.section, args.conf
            )
            print(msg)
            parser.print_help()
            sys.exit(1)
        parsed_config.update(config[args.section])
    
    for option in REQUIRED:
        if option not in parsed_config:
            msg = (
                "'{0}' option is required to be"
                " set in '{1}' for build_fiddle work"
            ).format(option, args.conf)
            print(msg, end="\n\n")
            parser.print_help()
            sys.exit(1)
    
    for file in FILES:
        with open(file, "r") as f:
            lines = []
            for line in f:
                if re.search("{\!?\w+}", line, re.MULTILINE):
                    line = line.format(**parsed_config)
                lines.append(line)
        with open(file, "w") as f:
            f.write("".join(lines))
    
    for dir_ in MKDIRS:
        try:
            os.makedirs(dir_.format(**parsed_config))
        except OSError:
            pass
    
    for file in TOUCH:
        file = file.format(**parsed_config)
        with open(file, "w"):
            pass
    
    for file in MV:
        file, dest = file
        shutil.move(file, dest.format(**parsed_config))
    
    return 0


if __name__ == "__main__":
    main()
