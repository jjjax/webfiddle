'use strict';


// Generic options.
const ON_ERROR = console.log;
const ADD_SOURCEMAPS = true;


// Jade/Pug options.
const JADE_PRETTY = true;


// JavaScript options.
const JS_UGLIFY = false;
let INCLUDE_FROM_THIRD_PARTY = [
    // Examples. Set your paths relatively 'project_root/src/third_party/'.
    // So, to inclute /project_root/src/third_party/jquery/dist/jquery.js/,
    //  simply write 'jquery/dist/jquery.js'.
    // Try to execute bower install --save jquery normalize.css, uncomment
    //  following two lines and save and rerun gulp:
    // "jquery/dist/jquery.js",
    // "normalize-css/normalize.css"

];
for (let i=0; i < INCLUDE_FROM_THIRD_PARTY.length; i++)
    INCLUDE_FROM_THIRD_PARTY[i] = "{src_name}/third_party/" + INCLUDE_FROM_THIRD_PARTY[i];


// Stylus options.
const CSS_INCLUDE = true;
const CSS_UGLIFY = false;
const DO_PREFIX = false;
const DO_PREFIX_CASCADE = false;
const PREFIX = ['last 2 versions'];


// Dev deps.
let gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    jade = require('gulp-jade'),
    sourcemaps = require('gulp-sourcemaps'),
    prefixer = require('gulp-autoprefixer'),
    rimraf = require('rimraf'),
    uglify = require('gulp-uglify');


// Paths.
let path = {
    dist: {
        html: '{dist_name}/',
        js: '{dist_name}/js/',
        css: '{dist_name}/css/',
        img: '{dist_name}/img/',
        fonts: '{dist_name}/fonts/',
        third_party: '{dist_name}/third_party/'
    },
    src: {
        html: '{src_name}/**/*.html',
        jade: ['{src_name}/jade/**/*.jade', '!{src_name}/jade/**/_*.jade', '!{src_name}/jade/{partials_name}/_*.jade'],
        stylus: ['{src_name}/stylus/**/*.styl', '!{src_name}/stylus/**/_*.styl', '!{src_name}/stylus/{partials_name}/_*.stylus'],
        js: ['{src_name}/js/**/*.js', '!{src_name}/js/{partials_name}/*.js'],
        img: '{src_name}/img/**/*.*',
        fonts: '{src_name}/fonts/**/*.*',
        third_party: INCLUDE_FROM_THIRD_PARTY
    },
    watch: {
        html: '{src_name}/**/*.html',
        jade: '{src_name}/jade/**/*.jade',
        stylus: '{src_name}/stylus/**/*.styl',
        js: '{src_name}/js/**/*.js',
        img: '{src_name}/img/**/*.*',
        fonts: '{src_name}/fonts/**/*.*'
    },
    clean: './{dist_name}/'
};


// Tasks.
gulp.task('html:build', function() {
    return gulp.src(path.src.html)
        .on('error', ON_ERROR)
        .pipe(gulp.dest(path.dist.html));
});

gulp.task('jade:build', function() {
    return gulp.src(path.src.jade)
        .pipe(jade({ pretty: JADE_PRETTY }))
        .on('error', ON_ERROR)
        .pipe(gulp.dest(path.dist.html));
});

gulp.task('stylus:build', function() {
    if (ADD_SOURCEMAPS) {
        return gulp.src(path.src.stylus)
            .pipe(sourcemaps.init())
            .pipe(stylus({
                compress: CSS_UGLIFY,
                'include css': CSS_INCLUDE
            }))
            .on('error', ON_ERROR)
            .pipe(prefixer({
                browsers: PREFIX,
                add: DO_PREFIX,
                cascade: DO_PREFIX_CASCADE
            }))
            .on('error', ON_ERROR)
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.dist.css));
    } else {
        return gulp.src(path.src.stylus)
            .pipe(stylus({
                compress: CSS_UGLIFY,
                'include css': CSS_INCLUDE
            }))
            .on('error', ON_ERROR)
            .pipe(prefixer({
                browsers: PREFIX,
                add: DO_PREFIX,
                cascade: DO_PREFIX_CASCADE
            }))
            .on('error', ON_ERROR)
            .pipe(gulp.dest(path.dist.css));
    }
});

gulp.task('js:build', function() {
    if (ADD_SOURCEMAPS) {
        return gulp.src(path.src.js)
            .pipe(sourcemaps.init())
            .pipe(uglify({
                mangle: JS_UGLIFY,
                compress: JS_UGLIFY,
                output: {
                    beautify: JS_UGLIFY ? false : true
                }
            }))
            .on('error', ON_ERROR)
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.dist.js));
    } else {
        return gulp.src(path.src.js)
            .pipe(uglify({
                mangle: JS_UGLIFY,
                compress: JS_UGLIFY,
                output: {
                    beautify: JS_UGLIFY ? false : true
                }
            }))
            .on('error', ON_ERROR)
            .pipe(gulp.dest(path.dist.js));
    }
});

gulp.task('image:build', function() {
    return gulp.src(path.src.img)
        .on('error', ON_ERROR)
        .pipe(gulp.dest(path.dist.img));
});

gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .on('error', ON_ERROR)
        .pipe(gulp.dest(path.dist.fonts));
});

gulp.task('third_party:build', function() {
    return gulp.src(path.src.third_party)
        .on('error', ON_ERROR)
        .pipe(gulp.dest(path.dist.third_party));
});

gulp.task('clean', function(cb) {
    return rimraf(path.clean, cb);
});


// Default tasks and watchers.
gulp.task('build', [
    'html:build',
    'jade:build',
    'stylus:build',
    'js:build',
    'fonts:build',
    'image:build',
    'third_party:build'
]);

gulp.task('watch', function() {
   gulp.watch(path.src.html, ['html:build']);
   gulp.watch(path.src.jade, ['jade:build']);
   gulp.watch(path.src.stylus, ['stylus:build']);
   gulp.watch(path.src.js, ['js:build']);
   gulp.watch(path.src.img, ['image:build']);
   gulp.watch(path.src.fonts, ['fonts:build']);
   gulp.watch(path.src.third_party, ['third_party:build']);
});

gulp.task('default', ['build', 'watch']);
